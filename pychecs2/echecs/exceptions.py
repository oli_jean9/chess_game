# -*- coding: utf-8 -*-
"""Ce module contient les exceptions qui pourront être soulevées lors d'une partie.

"""

class PieceMauvaiseCouleur(Exception):
    """Classe héritant d'Exception qui sera levée lorsque la pièce sélectionnée n'est pas de la bonne couleur.

    """

    pass

class DeplacementInvalide(Exception):
    """Classe héritant d'Exception qui sera levée lorsque le déplacement tenté est invalide.

    """

    pass

class DeplacementImpossibleEchec(Exception):
    """Classe héritant d'Exception qui sera levée lorsque le déplacement tenté est invalide, car il met le roi du joueur actif en échec
        ou que le déplacement tenté ne permet pas de sortir le roi d'une situation d'échec.

    """

    pass