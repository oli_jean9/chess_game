# -*- coding: utf-8 -*-
"""Ce module contient une classe contenant les informations sur une partie d'échecs,
dont un objet échiquier (une instance de la classe Echiquier).

"""
from pychecs2.echecs.echiquier import Echiquier
from pychecs2.echecs.piece import Pion
import copy

class Partie:
    """La classe Partie contient les informations sur une partie d'échecs, c'est à dire un échiquier, puis
    un joueur actif (blanc ou noir). Des méthodes sont disponibles pour faire avancer la partie et interagir
    avec l'utilisateur.

    Attributes:
        joueur_actif (str): La couleur du joueur actif, 'blanc' ou 'noir'.
        joueur_inactif (str): La couleur du joueur inactif, 'blanc' ou 'noir'.
        echiquier (Echiquier): L'échiquier sur lequel se déroule la partie.
        historique_deplacements (list): Liste contenant, dans l'ordre, l'ensemble des déplacements effectués (DeplacementEffectuer).

    """
    def __init__(self):
        # Le joueur débutant une partie d'échecs est le joueur blanc.
        self.joueur_actif = 'blanc'
        self.joueur_inactif = 'noir'

        # Création d'une instance de la classe Echiquier, qui sera manipulée dans les méthodes de la classe.
        self.echiquier = Echiquier()
        self.historique_deplacements = []

    def determiner_gagnant(self):
        """Détermine la couleur du joueur gagnant, s'il y en a un. Pour déterminer si un joueur est le gagnant,
        le roi de la couleur adverse doit être absente de l'échiquier.

        Returns:
            str: 'blanc' si le joueur blanc a gagné, 'noir' si c'est plutôt le joueur noir, et 'aucun' si aucun
                joueur n'a encore gagné.

        """
        if not self.echiquier.roi_de_couleur_est_dans_echiquier('noir'):
            return 'blanc'

        if not self.echiquier.roi_de_couleur_est_dans_echiquier('blanc'):
            return 'noir'

        if self.echec_et_mat():
            return self.joueur_actif

        return 'aucun'

    def partie_terminee(self):
        """Vérifie si la partie est terminée. Une partie est terminée si un gagnant peut être déclaré.

        Returns:
            bool: True si la partie est terminée, et False autrement.

        """

        if self.determiner_gagnant() == 'aucun':
            return False

        return True

    def deplacement_impossible_echec(self, position_source, position_cible, test_fin_de_partie='non'):

        """Vérifie si un déplacement est impossible en raison d'une mise en échec.

        Args:
            position_source(str): Position source
            position_cible(str): Position cible
            test_fin_de_partie (str): "non" par défaut. Mettre oui si la fonction est utilisée dans le but de tester
                                        une partie nulle par "pat" ou un échec et mat

        Returns:
            Exception(DeplacementImpossibleEchec): Une exception est soulevée si le déplacement est impossible.
            Cette exception sera captée, au besoin, lors d'un déplacement.

        """

        dictionnaire_pieces_copy = copy.deepcopy(self.echiquier.dictionnaire_pieces)
        piece_blanche_mangees_copy = self.echiquier.pieces_blanche_mangees.copy()
        piece_noir_mangees_copy = self.echiquier.pieces_noir_mangees.copy()
        self.echiquier.deplacer(position_source, position_cible)

        if test_fin_de_partie == 'non':
            couleur_test = self.joueur_inactif

        else:
            couleur_test = self.joueur_actif

        if self.echiquier.roi_est_en_echec(couleur_test):

            self.echiquier.dictionnaire_pieces = dictionnaire_pieces_copy
            self.echiquier.pieces_blanche_mangees = piece_blanche_mangees_copy
            self.echiquier.pieces_noir_mangees = piece_noir_mangees_copy

            return True

        else:
            self.echiquier.dictionnaire_pieces = dictionnaire_pieces_copy
            self.echiquier.pieces_blanche_mangees = piece_blanche_mangees_copy
            self.echiquier.pieces_noir_mangees = piece_noir_mangees_copy

            return False

    def echec_et_mat(self):
        """Cette méthode permet de détecter un échec et mat.

        """

        if self.echiquier.roi_est_en_echec(self.joueur_actif):
            for item in self.echiquier.dictionnaire_pieces.items():
                position_source = item[0]
                piece = item[1]

                if piece.couleur == self.joueur_inactif:
                    deplacement_possible_liste = self.echiquier.deplacements_possible(position_source)

                    for position_cible in deplacement_possible_liste:
                        if not self.deplacement_impossible_echec(position_source, position_cible, 'oui'):
                            return False
            return True

        else:
            return False



    def nulle_type_pat(self):
        """Cette méthode permet de détecter une partie nulle de type "pat".

        """

        if not self.echiquier.roi_est_en_echec(self.joueur_actif):
            for item in self.echiquier.dictionnaire_pieces.items():
                position_source = item[0]
                piece = item[1]

                if piece.couleur == self.joueur_inactif:
                    deplacement_possible_liste = self.echiquier.deplacements_possible(position_source)

                    for position_cible in deplacement_possible_liste:
                        if not self.deplacement_impossible_echec(position_source, position_cible, 'oui'):
                            return False

            return True

        return False

    def joueur_suivant(self):
        """Change le joueur actif et le joueur_inactif: passe de blanc à noir, ou de noir à blanc, selon la couleur du joueur actif.

        """
        if self.joueur_actif == 'blanc':
            self.joueur_actif = 'noir'
            self.joueur_inactif = 'blanc'
        else:
            self.joueur_actif = 'blanc'
            self.joueur_inactif = 'noir'


    def promotion_pion(self):
        """Determine si un joueur est en position de faire la promotion d'un pion. Si c'est le cas, on retourne la position sinon None.

        """

        for item in self.echiquier.dictionnaire_pieces.items():

            position = item[0]
            piece = item[1]

            rangee_promotion_blanc = ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
            rangee_promotion_noir = ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1']

            if isinstance(piece, Pion):

                if piece.couleur == 'blanc' and position in rangee_promotion_blanc:
                    return position

                if piece.couleur == 'noir' and position in rangee_promotion_noir:
                    return position

        return None


class DeplacementEffectuer:

    """La classe DeplacementEffectuer contient l'ensemble des informations en lien avec un déplacement effectué.

    Attributes:
        position_source (str): Position source
        position_cible (str): Position cible
        piece_deplacer (Piece): Pièce deplacée
        piece_mangee (Piece ou None): Pièce mangée le cas échéant ou None si aucune pièce n'a été mangée

    """

    def __init__(self, position_source, position_cible, piece_deplacer, piece_mangee=None):

        self.position_source = position_source
        self.position_cible = position_cible
        self.piece_deplacer = piece_deplacer
        self.piece_mangee = piece_mangee




