# -*- coding: utf-8 -*-
""" Ce module contient les classes qui seront nécessaire à l'affichage de l'ensemble de l'interface du jeu d'échec

"""

from tkinter import NSEW, NW, W, EW, Canvas, Label, Tk, Button, END, Frame, Listbox, Toplevel, Entry, messagebox, DISABLED
from PIL import Image, ImageTk
import webbrowser
import tkinter.font as TkFont
import pickle
from pychecs2.echecs.exceptions import DeplacementInvalide, PieceMauvaiseCouleur, DeplacementImpossibleEchec
from pychecs2.echecs.partie import Partie, DeplacementEffectuer
from pychecs2.echecs.piece import Dame, Tour, Fou, Cavalier


class CanvasEchiquier(Canvas):
    """Classe héritant d'un Canvas, et affichant un échiquier qui se redimensionne automatique lorsque
    la fenêtre est étirée.

    """
    def __init__(self, parent, n_pixels_par_case, couleur):

        self.couleur_select = couleur

        # Nombre de lignes et de colonnes.
        self.n_lignes = 8
        self.n_colonnes = 8

        # Noms des lignes et des colonnes.
        self.chiffres_rangees = ['1', '2', '3', '4', '5', '6', '7', '8']
        self.chiffres_rangees_inverse = ['8', '7', '6', '5', '4', '3', '2', '1']
        self.lettres_colonnes = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

        # Nombre de pixels par case, variable.
        self.n_pixels_par_case = n_pixels_par_case

        # Appel du constructeur de la classe de base (Canvas).
        # La largeur et la hauteur sont déterminés en fonction du nombre de cases.
        super().__init__(parent, width=self.n_lignes * n_pixels_par_case,
                         height=self.n_colonnes * self.n_pixels_par_case)

        # Dictionnaire contenant les pièces. Vous devinerez, si vous réutilisez cette classe dans votre TP4,
        # qu'il fandra adapter ce code pour plutôt utiliser la classe Echiquier. MODIFIER

        self.partie = Partie()

        # On fait en sorte que le redimensionnement du canvas redimensionne son contenu. Cet événement étant également
        # généré lors de la création de la fenêtre, nous n'avons pas à dessiner les cases et les pièces dans le
        # constructeur.
        self.bind('<Configure>', self.redimensionner)

        self.source = ''
        self.cible = ''


    def dessiner_cases(self):
        """Méthode qui dessine les cases de l'échiquier.

        """
        for i in range(self.n_lignes):
            for j in range(self.n_colonnes):
                debut_ligne = i * self.n_pixels_par_case
                fin_ligne = debut_ligne + self.n_pixels_par_case
                debut_colonne = j * self.n_pixels_par_case
                fin_colonne = debut_colonne + self.n_pixels_par_case

                # On détermine la couleur.
                if (i + j) % 2 == 0:
                    couleur = 'white'
                else:
                    couleur = self.couleur_select

                # On dessine le rectangle. On utilise l'attribut "tags" pour être en mesure de récupérer les éléments
                # par la suite.
                self.create_rectangle(debut_colonne, debut_ligne, fin_colonne, fin_ligne, fill=couleur, tags='case')


    def dessiner_pieces(self):
        """Méthode qui dessine les pièces de l'échiquier.

        """

        # Pour tout paire position, pièce:
        for position, piece in self.partie.echiquier.dictionnaire_pieces.items():
            # On dessine la pièce dans le canvas, au centre de la case. On utilise l'attribut "tags" pour être en
            # mesure de récupérer les éléments dans le canvas.
            coordonnee_y = (self.n_lignes - self.chiffres_rangees.index(position[1]) - 1) * self.n_pixels_par_case + self.n_pixels_par_case // 2
            coordonnee_x = self.lettres_colonnes.index(position[0]) * self.n_pixels_par_case + self.n_pixels_par_case // 2
            self.create_text(coordonnee_x, coordonnee_y, text=piece,
                             font=('Deja Vu', self.n_pixels_par_case//2), tags='piece')


    def redimensionner(self, event):
        """Méthode qui redimensionne l'échiquier.

        """

        # Nous recevons dans le "event" la nouvelle dimension dans les attributs width et height. On veut un damier
        # carré, alors on ne conserve que la plus petite de ces deux valeurs.
        nouvelle_taille = min(event.width, event.height)

        # Calcul de la nouvelle dimension des cases.
        self.n_pixels_par_case = nouvelle_taille // self.n_lignes

        # On supprime les anciennes cases et on ajoute les nouvelles.
        self.delete('case')
        self.dessiner_cases()

        # On supprime les anciennes pièces et on ajoute les nouvelles.
        self.delete('piece')
        self.dessiner_pieces()


    def update_piece(self):
        """Méthode qui met à jour l'affichage des pièces.

        """

        self.delete('piece')
        self.dessiner_pieces()


    def update_case(self):
        """Méthode qui met à jour l'affichage des cases.

        """

        self.delete('cases')
        self.dessiner_cases()


    def selection_case(self, position):

        """Méthode qui dessine les cases de l'échiquier en mettant de couleur différente la position sélectionnée ainsi
        que les deplacement possibles pour la pièce se trouvant à la position sélectionnée

        Args:
            position (str): Position sélectionnée

        """

        self.delete('case')
        self.position = position

        self.id_colonne = self.lettres_colonnes.index(self.position[0])
        self.id_rangee = self.chiffres_rangees_inverse.index(self.position[1])
        self.deplacements_valide = self.partie.echiquier.deplacements_possible(self.position)

        for i in range(self.n_lignes):
            for j in range(self.n_colonnes):
                debut_ligne = i * self.n_pixels_par_case
                fin_ligne = debut_ligne + self.n_pixels_par_case
                debut_colonne = j * self.n_pixels_par_case
                fin_colonne = debut_colonne + self.n_pixels_par_case

                # On détermine la couleur.
                if i == self.id_rangee and j == self.id_colonne:
                    couleur = 'yellow'

                elif (i + j) % 2 == 0:
                    couleur = 'white'

                else:
                    couleur = self.couleur_select

                for position in self.deplacements_valide:
                    self.id_colonne_deplacement = self.lettres_colonnes.index(position[0])
                    self.id_rangee_deplacement = self.chiffres_rangees_inverse.index(position[1])

                    if i == self.id_rangee_deplacement and j == self.id_colonne_deplacement:
                        couleur = 'yellow'

                # On dessine le rectangle. On utilise l'attribut "tags" pour être en mesure de récupérer les éléments
                # par la suite.
                self.create_rectangle(debut_colonne, debut_ligne, fin_colonne, fin_ligne, fill=couleur, tags='case')


class Fenetre(Tk):
    """Classe héritant d'une fenêtre Tk, et qui affichera l'interface de jeu. Cette interface comprends un objet CanvasEchiquier
    ainsi que l'ensemble des éléments présents dans l'interface de jeu.

    """

    def __init__(self):
        
        super().__init__()

        # Couleur par défaut de l'échiquier
        self.color_echiquier = 'gray'

        # Choix d'une couleur d'échiquier
        color_w = ColorChoice(self)
        self.wait_window(color_w)

        # Dimension par défaut de la fenêtre de jeu
        self.geometry('920x920')

        # Nom de la fenêtre.
        self.title("Échiquier")

        # La position sélectionnée.
        self.position_selectionnee = None

        # Truc pour le redimensionnement automatique des éléments de la fenêtre.
        self.grid_columnconfigure(0, weight=1)
        self.grid_columnconfigure(1, weight=0)
        self.grid_rowconfigure(0, weight=0)
        self.grid_rowconfigure(1, weight=1)
        self.grid_rowconfigure(2, weight=0)

        # Entête de la fenêtre de jeu
        self.entete = Label(self, text='La partie est commencée!')
        self.entete.grid(row=0, padx=20, pady=20, columnspan=2, sticky=NSEW)

        entete_font = TkFont.Font(size=20)
        self.entete.config(font=entete_font)

        # Création du canvas échiquier.
        self.canvas_echiquier = CanvasEchiquier(self, 60, self.color_echiquier)
        self.canvas_echiquier.grid(row=1, column=0, sticky=NSEW, padx=15, pady=15)

        # Cadre d'informations concernant les déplacements effectués et autres options de jeu
        self.info_move_frame = Frame()
        self.info_move_frame.grid(row=1, column=1, sticky=NSEW)

        # Affichage du joueur actif
        self.tour_label = Label(self.info_move_frame, text= f"Tour des: {self.canvas_echiquier.partie.joueur_actif.upper()}")
        self.tour_label.grid(row=0, column=0, padx=10, pady=10)

        # Affichage des déplacements effectués et boutton annuler deplacement
        self.move_label = Label(self.info_move_frame, text='Historique des déplacements')
        self.move_label.grid(row=1, column=0, padx=10, pady=10)


        self.annuler_dernier_deplacement_button = Button(self.info_move_frame, text="Annuler déplacement", state=DISABLED, command=self.annuler_dernier_deplacement)
        self.annuler_dernier_deplacement_button.grid(row=2, column=0, padx=10, pady=10)

        move_listbox_font = TkFont.Font(size=12)
        self.move_listbox = Listbox(self.info_move_frame, height=24, font=move_listbox_font)
        self.move_listbox.grid(row=3, column=0, padx=10, pady=5, sticky=NSEW)

        # Boutton enregistrer partie
        self.save_button = Button(self.info_move_frame, text='Sauvegarder', command=self.save_game)
        self.save_button.grid(row=4,column=0, padx=5, pady=5)

        # Boutton charger partie
        self.load_button = Button(self.info_move_frame, text='Charger', command=self.load_game_fenetre)
        self.load_button.grid(row=5, column=0, padx=5, pady=5)

        # Nouvelle partie
        self.new_game_button = Button(self.info_move_frame, text='Nouvelle Partie', command=self.start_new_game)
        self.new_game_button.grid(row=6, column=0, padx=5, pady=5)

        # Affichage des pièces mangées
        self.piece_mangee_label = Label(self, text="PIÈCES MANGÉES:")
        self.piece_mangee_label.grid(row=2, sticky=W)

        piece_mangee_font = TkFont.Font(size=18)

        self.piece_blanche_mangee_label = Label(self, text=self.canvas_echiquier.partie.echiquier.pieces_blanche_mangees)
        self.piece_blanche_mangee_label.grid(row=3, sticky=W, padx=5, pady=5)
        self.piece_blanche_mangee_label.config(font=piece_mangee_font)

        self.piece_noir_mangee_label = Label(self,text=self.canvas_echiquier.partie.echiquier.pieces_noir_mangees)
        self.piece_noir_mangee_label.grid(row=4, sticky=W, padx=5, pady=5)
        self.piece_noir_mangee_label.config(font=piece_mangee_font)

        # Ajout d'une étiquette d'information.
        self.messages = Label(self)
        self.messages.grid()

        self.canvas_echiquier.bind('<Button-1>', self.selectionner)


    def update_pieces_mangees(self):
        """Méthode qui met à jour l'affichage des pièces mangées.

        """

        self.piece_blanche_mangee_label['text'] = self.canvas_echiquier.partie.echiquier.pieces_blanche_mangees
        self.piece_noir_mangee_label['text'] = self.canvas_echiquier.partie.echiquier.pieces_noir_mangees


    def update_hitorique_deplacements_listbox(self):
        """Méthode qui met à jour l'affichage de l'historique des déplacements.

        """

        self.move_listbox.delete(0, END)

        for deplacement in self.canvas_echiquier.partie.historique_deplacements:
            if deplacement.piece_mangee is not None:
                self.move_listbox.insert(END,f'{deplacement.piece_deplacer}: {deplacement.position_source} -> {deplacement.position_cible} x {deplacement.piece_mangee}')
            else:
                self.move_listbox.insert(END,f'{deplacement.piece_deplacer}: {deplacement.position_source} -> {deplacement.position_cible}')

        self.move_listbox.see(END)


    def update_joueur_actif(self):
        """Méthode qui met à jour l'étiquette indiquant le joueur actif.

        """

        self.tour_label['text'] = f"Tour des: {self.canvas_echiquier.partie.joueur_actif.upper()}"


    def clear_position(self):
        """Méthode qui réinitialise la position source et la position cible

        """

        self.canvas_echiquier.source = ''
        self.canvas_echiquier.cible = ''


    def update_interface(self):
        """Méthode qui met à jour l'ensemble de l'interface de jeu.

        """

        self.update_pieces_mangees()
        self.update_hitorique_deplacements_listbox()
        self.update_joueur_actif()
        self.clear_position()
        self.canvas_echiquier.dessiner_cases()
        self.canvas_echiquier.update_piece()


    def selectionner(self, event):
        """Méthode qui permet de selectionner des positions sur l'échiquier.

        """

        # On trouve le numéro de ligne/colonne en divisant les positions en y/x par le nombre de pixels par case.
        ligne = event.y // self.canvas_echiquier.n_pixels_par_case
        colonne = event.x // self.canvas_echiquier.n_pixels_par_case

        try:
            position = "{}{}".format(self.canvas_echiquier.lettres_colonnes[colonne], int(self.canvas_echiquier.chiffres_rangees[self.canvas_echiquier.n_lignes - ligne - 1]))

            self.position_selectionnee = position

            # On récupère l'information sur la pièce à l'endroit choisi. Notez le try...except!
            piece = self.canvas_echiquier.partie.echiquier.dictionnaire_pieces[position]

            # On change la valeur de l'attribut position_selectionnee.
            if piece.couleur == self.canvas_echiquier.partie.joueur_actif:
                self.canvas_echiquier.source = self.position_selectionnee

                self.messages['foreground'] = 'black'
                self.messages['text'] = 'Pièce sélectionnée : {} à la position {}.'.format(piece, self.position_selectionnee)

                self.canvas_echiquier.selection_case(self.canvas_echiquier.source)
                self.canvas_echiquier.update_piece()

            if not piece.couleur == self.canvas_echiquier.partie.joueur_actif and not self.canvas_echiquier.source == '':
                self.canvas_echiquier.cible = self.position_selectionnee

            if not piece.couleur == self.canvas_echiquier.partie.joueur_actif and self.canvas_echiquier.source == '':
                raise PieceMauvaiseCouleur

        except IndexError:
            self.messages['foreground'] = 'red'
            self.messages['text'] = "Cliquez sur l'échiquier pour sélectionner une position"

        except UnboundLocalError:
            self.messages['foreground'] = 'red'
            self.messages['text'] = "Cliquez sur l'échiquier pour sélectionner une position"

        except KeyError:
            if self.canvas_echiquier.source == '':
                self.messages['foreground'] = 'red'
                self.messages['text'] = 'Erreur: Aucune pièce à cet endroit.'
            else:
                self.canvas_echiquier.cible = self.position_selectionnee

        except PieceMauvaiseCouleur:
            self.messages['foreground'] = 'red'
            self.messages['text'] = "Ce n'est pas votre tour"

        if not self.canvas_echiquier.source == '' and not self.canvas_echiquier.cible == '':
            self.deplacer()


    def annuler_dernier_deplacement(self):
        """Méthode qui permet d'annuler le dernier déplacement effectué.

        """

        dernier_deplacement = self.canvas_echiquier.partie.historique_deplacements[-1]

        # Rétablie la position d'origine de la pièce déplacée
        self.canvas_echiquier.partie.echiquier.dictionnaire_pieces[dernier_deplacement.position_source] = dernier_deplacement.piece_deplacer

        # Rétablie la position de la pièce mangée le cas échéant
        if dernier_deplacement.piece_mangee is not None:
            self.canvas_echiquier.partie.echiquier.dictionnaire_pieces[dernier_deplacement.position_cible] = dernier_deplacement.piece_mangee

            # Rétablie la liste des pièces mangées
            if dernier_deplacement.piece_mangee.couleur == 'blanc':
                del self.canvas_echiquier.partie.echiquier.pieces_blanche_mangees[-1]
            else:
                del self.canvas_echiquier.partie.echiquier.pieces_noir_mangees[-1]
        else:
            del self.canvas_echiquier.partie.echiquier.dictionnaire_pieces[dernier_deplacement.position_cible]

        # Supprime le déplacement de la liste d'historique des déplacements
        del self.canvas_echiquier.partie.historique_deplacements[-1]

        # Si aucun déplacement n'a été éffectué, le boutton annuler déplacement est désactivé
        if len(self.canvas_echiquier.partie.historique_deplacements) == 0:
            self.annuler_dernier_deplacement_button['state'] = DISABLED

        # Rétablie le message de roi en échec au besoin
        if self.canvas_echiquier.partie.echiquier.roi_est_en_echec(self.canvas_echiquier.partie.joueur_actif):
            self.messages.config(text=f"Le roi {self.canvas_echiquier.partie.joueur_inactif.upper()} est en échec!", fg='red')
        else:
            self.messages['text'] = ''

        # Retour au bon joueur actif
        self.canvas_echiquier.partie.joueur_suivant()

        # Mise à jour de l'interface de jeu
        self.update_interface()


    def deplacer(self):
        """Méthode qui effectue les deplacements ou retourne les messages approppriés en cas de déplacement invalide.

        """

        try:
            # Une exception est levée si le déplacement est impossible en raison d'un roi en échec
            if self.canvas_echiquier.partie.deplacement_impossible_echec(self.canvas_echiquier.source, self.canvas_echiquier.cible):
                raise DeplacementImpossibleEchec

            piece_deplacer = self.canvas_echiquier.partie.echiquier.recuperer_piece_a_position(self.canvas_echiquier.source)
            piece_mangee = self.canvas_echiquier.partie.echiquier.recuperer_piece_a_position(self.canvas_echiquier.cible)

            # Effectuer le déplacement et activer le boutton annuler déplacement
            self.canvas_echiquier.partie.echiquier.deplacer(self.canvas_echiquier.source, self.canvas_echiquier.cible)
            self.annuler_dernier_deplacement_button['state'] = 'normal'

            # Ajout du déplacement à l'historique
            deplacement_effectuer = DeplacementEffectuer(self.canvas_echiquier.source, self.canvas_echiquier.cible, piece_deplacer, piece_mangee)
            self.canvas_echiquier.partie.historique_deplacements.append(deplacement_effectuer)

            # Promotion du Pion

            if self.canvas_echiquier.partie.promotion_pion() is not None:
                position_promotion = self.canvas_echiquier.partie.promotion_pion()
                promotion = PromotionPion(self)
                self.wait_window(promotion)

                self.canvas_echiquier.partie.echiquier.dictionnaire_pieces[position_promotion] = promotion.piece_promotion

            # Mise à jour de l'interface
            self.update_interface()
            self.messages['text'] = ''

            # Partie nulle de type "pat"
            if self.canvas_echiquier.partie.nulle_type_pat():
                self.messages.config(text=f"La partie est nulle...personne ne gagne...", fg='red')
                EndGame(self,'non')

            # Affichage du message en cas de mise en échec
            if self.canvas_echiquier.partie.echiquier.roi_est_en_echec(self.canvas_echiquier.partie.joueur_actif):

                self.messages.config(text=f"Le roi {self.canvas_echiquier.partie.joueur_inactif.upper()} est en échec!", fg='red')

            # Fin de partie
            if self.canvas_echiquier.partie.echec_et_mat():
                self.messages.config(text=f"ÉCHEC ET MAT! LE JOUEUR {self.canvas_echiquier.partie.determiner_gagnant().upper()} A GAGNÉ!", fg='green')
                EndGame(self)


            # Tour suivant:
            self.clear_position()
            self.canvas_echiquier.partie.joueur_suivant()
            self.update_joueur_actif()

        except DeplacementInvalide:

            self.clear_position()
            self.canvas_echiquier.update_case()
            self.canvas_echiquier.update_piece()
            self.messages.config(text="Le déplacement est invalide", fg='red')

        except DeplacementImpossibleEchec:

            self.clear_position()
            self.canvas_echiquier.update_case()
            self.canvas_echiquier.update_piece()
            self.messages.config(text="MISE EN ÉCHEC! Déplacement invalide!", fg='red')


    def save_game(self):
        """Méthode qui permet d'enregistrer une partie.

        """
        try:
            sauvegarde = SaveLoadGame(self, action='Sauvegarder')
            self.wait_window(sauvegarde)

            sauvegarde_file = sauvegarde.name+'.pkl'

            with open(sauvegarde_file, 'wb') as file:
                pickle.dump(self.canvas_echiquier.partie, file, pickle.HIGHEST_PROTOCOL)

        except AttributeError:
            return None


    def load_game_fenetre(self):
        """Méthode qui permet de charger une partie.

        """

        try:
            load = SaveLoadGame(self)
            self.wait_window(load)

            load_file = load.name+'.pkl'

            with open(load_file, 'rb') as file:
                partie = pickle.load(file)

            self.canvas_echiquier.partie = partie
            self.annuler_dernier_deplacement_button['state'] = 'normal'
            self.update_interface()

            # Rétablie le message de roi en échec au besoin
            if self.canvas_echiquier.partie.echiquier.roi_est_en_echec(self.canvas_echiquier.partie.joueur_actif):
                self.messages.config(text=f"Le roi {self.canvas_echiquier.partie.joueur_inactif.upper()} est en échec!", fg='red')
            else:
                self.messages['text'] = ''

        except FileNotFoundError:
            messagebox.showerror('Erreur', "Le fichier n'existe pas")

        except AttributeError:
            pass


    def start_new_game(self):

        self.destroy()
        Fenetre().mainloop()


class Menu(Canvas):
    """Classe héritant d'un Canvas, et qui affichera les élements de l'écran d'acceuil.

    """

    def __init__(self):
        super().__init__()

        #Création du Canvas
        self.menu_canvas = Canvas(width=960, height=640, bg='black')
        self.menu_canvas.pack()

        # Image de fond
        self.background_image = Image.open('menu_image.jpg')
        self.background_image_copy = self.background_image.copy()

        self.photo = ImageTk.PhotoImage(self.background_image)
        self.menu_canvas.create_image(0,0,image=self.photo,anchor=NW)

        # Button
        self.jouer_button = Button(self.menu_canvas, text="Démarrer une nouvelle partie")
        self.jouer_button.w = self.menu_canvas.create_window(480,270,window=self.jouer_button)

        self.charger_partie_button = Button(self.menu_canvas, text="Charger une partie")
        self.charger_partie_button.w = self.menu_canvas.create_window(480, 300, window=self.charger_partie_button)

        self.rules_button = Button(self.menu_canvas, text="Afficher les règles du jeu")
        self.rules_button.bind("<Button-1>", lambda e: self.callback("www.iechecs.com/regles.htm"))
        self.rules_button.w = self.menu_canvas.create_window(480,330,window=self.rules_button)


    def callback(self, url):
        """Méthode qui permet d'ouvrir une page web.

        """

        webbrowser.open(url)


class MasterMenu(Tk):
    """Classe héritant d'une fenêtre Tk, à laquelle sera intégré un objet de la classe Menu.

    """

    def __init__(self):
        super().__init__()

        # Paramètre de dimensionnement de la fenêtre
        self.pack_propagate(0)
        self.config(width=960, height=640)
        self.resizable(False, False)

        # Création d'un canvas Menu
        canvas_menu = Menu()

        # Définition de la commande des boutton du canvas
        canvas_menu.jouer_button.config(command=self.start_game)
        canvas_menu.charger_partie_button.config(command=self.load_game_menu)

        # Intégration d'un canvas Menu à la fenêtre
        canvas_menu.pack()

    def start_game(self):
        """Méthode qui permet de commencer une partie.

        """

        self.destroy()
        game_interface = Fenetre()
        game_interface.mainloop()

    def load_game_menu(self):
        """Méthode qui permet de charger une partie.

        """

        self.destroy()
        game_interface = Fenetre()
        game_interface.load_game_fenetre()
        game_interface.mainloop()


class EndGame(Toplevel):
    """Classe héritant de Toplevel, qui s'affiche lorsque la partie est terminée. Elle donne l'option de rejouer ou de quitter.

        Attributes:
            master (Tk): Fenêtre à laquelle elle se rattache
            joueur_gagnant (str): Couleur du joueur gagnant

    """

    def __init__(self, master, gagnant='oui'):
        super().__init__(master)

        # On prend le contrôle de l'application
        self.master = master
        self.transient(master)
        self.grab_set()

        if gagnant == 'oui':
            joueur_gagnant = self.master.canvas_echiquier.partie.determiner_gagnant().upper()

            Label(self, text=f"ÉCHEC ET MAT! La partie est terminée!\nLe joueur {joueur_gagnant} a gagné!").grid(row=0, columnspan=2, padx=10, pady=10)

        else:
            Label(self, text=f"La partie est NULLE...personne ne gagne...").grid(row=0, columnspan=2, padx=10, pady=10)

        # Rejouer
        self.bouton_rejouer = Button(self, text="Rejouer", command=self.rejouer)
        self.bouton_rejouer.grid(row=1, column=0, padx=10, pady=10)

        # Quitter
        self.bouton_quitter = Button(self, text="Quitter", command=self.master.destroy)
        self.bouton_quitter.grid(row=1, column=1, padx=10, pady=10)

    def rejouer(self):
        """Méthode qui permet de rejouer une partie.

        """

        self.master.destroy()
        Fenetre().mainloop()


class ColorChoice(Toplevel):
    """Classe héritant de Toplevel, qui s'affiche lorsque la partie commence. Elle donne l'option de changer la couleur de l'échiquier.

        Attributes:
            master (Tk): Fenêtre à laquelle elle se rattache
    """

    def __init__(self, master):
        super().__init__(master)

        self.master = master
        self.transient(master)
        self.grab_set()

        self.title("Choix de couleur de l'échiquier")
        Label(self, text="Veuillez choisir la couleur de votre échiquier").grid(padx=10, pady=10, columnspan=2)

        # Choix des couleurs
        Button(self, text='Gris', fg='grey', command=lambda: self.select_color('grey')).grid(padx=5,pady=5, row=1, column=0, sticky=EW)
        Button(self, text='Bleu', fg='dodger blue', command= lambda: self.select_color('dodger blue')).grid(padx=5, pady=5, row=2, column=0, sticky=EW)
        Button(self, text='Orange', fg='orange', command=lambda: self.select_color('orange')).grid(padx=5, pady=5, row=3, column=0, sticky=EW)
        Button(self, text='Jaune', fg='gold', command=lambda: self.select_color('gold')).grid(padx=5, pady=5, row=1, column=1, sticky=EW)
        Button(self, text='Vert', fg='lime green', command=lambda: self.select_color('lime green')).grid(padx=5, pady=5, row=2, column=1, sticky=EW)
        Button(self, text='Rose', fg='deep pink', command=lambda: self.select_color('deep pink')).grid(padx=5, pady=5,row=3,column=1,sticky=EW)


    def select_color(self, color):
        """Méthode qui permet d'associer la couleur choisie à la variable color_echiquier et de fermer la fenêtre.

        """

        # On sauvegarde le résultat.
        self.master.color_echiquier = color

        # On redonne le contrôle au parent.
        self.grab_release()
        self.master.focus_set()
        self.destroy()


class SaveLoadGame(Toplevel):
    """Classe héritant de Toplevel, qui s'affiche lorsque l'on propose à l'utilisateur de charger ou de sauvegarder une partie.

        Attributes:
            master (Tk): Fenêtre à laquelle elle se rattache
            action (str): Charger par défaut. Entrer Sauvegarder pour une sauvegarde

    """
    def __init__(self, master, action='Charger'):
        super().__init__(master)

        # Formatage de la fenêtre
        self.grid_columnconfigure(0, weight=1)
        self.geometry('200x175')

        # On prend le contrôle de l'application
        self.master = master
        self.transient(master)
        self.grab_set()

        self.title(action)

        Label(self, text=f'{action} un fichier').grid(padx=10, pady=10, row=0)
        Label(self, text=f"Entrez le nom du fichier:").grid(padx=5, pady=5, row=1)

        # Zone d'entrée de texte
        self.entree = Entry(self)
        self.entree.grid(padx=5, pady=5, row=2)

        # Boutton OK
        self.bouton_ok = Button(self, text=action, command=self.fermer)
        self.bouton_ok.grid(padx=10, pady=10,row=3)


    def fermer(self):
        """Méthode qui affecte l'attribut "name" à la valeur choisie, et fermer la fenêtre.

        """

        # On sauvegarde le résultat.
        self.name = self.entree.get()

        # On redonne le contrôle au parent.
        self.grab_release()
        self.master.focus_set()
        self.destroy()


class PromotionPion(Toplevel):
    """Classe héritant de Toplevel, qui s'affiche lorsque le joueur peut faire la promotion d'un de ses Pions

        Attributes:
            master (Tk): Fenêtre à laquelle elle se rattache

    """

    def __init__(self, master):

        super().__init__(master)

        self.master = master
        self.transient(master)
        self.grab_set()

        self.title('Promotion du pion')

        Label(self, text='Choisissez une pièce').grid(padx=10, pady=10, row=0)

        self.dame_button = Button(self, text='Dame', command=lambda: self.fermer(Dame(self.master.canvas_echiquier.partie.joueur_actif))).grid(padx=10, pady=10, row=1)
        self.tour_button = Button(self, text='Tour', command=lambda: self.fermer(Tour(self.master.canvas_echiquier.partie.joueur_actif))).grid(padx=10, pady=10, row=2)
        self.fou_button = Button(self, text='Fou', command=lambda: self.fermer(Fou(self.master.canvas_echiquier.partie.joueur_actif))).grid(padx=10, pady=10, row=3)
        self.cavalier_button = Button(self, text='Cavalier', command=lambda: self.fermer(Cavalier(self.master.canvas_echiquier.partie.joueur_actif))).grid(padx=10, pady=10, row=4)

    def fermer(self, piece):

        # On sauvegarde la pièce choisie.

        self.piece_promotion = piece

        # On redonne le contrôle au parent.
        self.grab_release()
        self.master.focus_set()
        self.destroy()
